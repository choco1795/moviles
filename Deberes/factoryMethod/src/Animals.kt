import java.lang.IllegalArgumentException

fun main(args: Array<String>){
    val catFactory = AnimalFactory.create<Cat>()
    val dogFactory = AnimalFactory.create<Dog>()
    val cat = catFactory.makeAnimal()
    val dog = dogFactory.makeAnimal()

    print(cat.toString())
}

interface Animal;

class Dog: Animal;
class Cat: Animal;
class Fish: Animal;

class DogFactory: AnimalFactory() {
    override fun makeAnimal(): Animal = Dog()
}

class CatFactory: AnimalFactory() {
    override fun makeAnimal(): Animal = Cat()
}

class FishFactory: AnimalFactory() {
    override fun makeAnimal(): Animal = Fish()
}

abstract class AnimalFactory {
    abstract fun makeAnimal(): Animal
    companion object {
        inline fun <reified T: Animal> create(): AnimalFactory =
            when(T:: class) {
                Dog:: class -> DogFactory()
                Cat:: class -> CatFactory()
                Fish:: class -> FishFactory()
                else -> throw IllegalArgumentException()
            }
    }
}

